
# Slack
- Empathy 
- Courtesy 
- Thriving 
- Craftsmanship 
- Playfulness 
- Solidarity



# Supercell
- **The best teams make the best games**
- **Small and independent cells**
- **Games that people will play for years**





# Quora
Quora's mission is to share and grow the world's knowledge.

## Quora's core values as an organization are:

- **Maximize long-term value.** We are building a product that we hope lasts forever. We want to be a strong and independent company. This long term focus guides all of the decisions we make.

- **Continuously learn and adapt**. We value experimentation, metrics-driven decisions, and speed of iteration. We push to production with every commit so we launch as soon as work is done and learn as quickly as possible.

- **Execute**. We value people who have great ideas but who also can quickly drive them all the way to implementation.
Hold high standards. We hire the best people. We set aggressive goals. We aspire to do our best work every day.
Be direct and respectful. We expect and welcome constant feedback. We trust each other to be open and straightforward.

## Quora's two key policy principles are:

- Make the page more helpful for people who are interested in the question.
- Be Nice, Be Respectful.
## other.
- **Mission First.** We place long-term, collective impact ahead of personal achievement.
- **Drive.** We aim high and do whatever it takes to get things done.
- **Agility.** We are nimble in our processes and systems, ready to adapt to a changing world.
- **Awareness.** We are rigorous in our decisions and candid in our communication.
- **Pragmatism.** We stay grounded in the outcomes that truly matter for our mission.

-----
### other thing to have in mind
- Critical Thinking.
- Growth Mindset.
- Empathy.
- Write everything down.
- Failiure is OK
- Do more.
- Dream better.
